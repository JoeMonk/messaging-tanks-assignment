/*******************************************
	ShellEntity.cpp

	Shell entity class
********************************************/

#include "ShellEntity.h"
#include "TankEntity.h"
#include "EntityManager.h"
#include "Messenger.h"

namespace gen
{

// Reference to entity manager from TankAssignment.cpp, allows look up of entities by name, UID etc.
// Can then access other entity's data. See the CEntityManager.h file for functions. Example:
//    CVector3 targetPos = EntityManager.GetEntity( targetUID )->GetMatrix().Position();
extern CEntityManager EntityManager;

// Messenger class for sending messages to and between entities
extern CMessenger Messenger;


/*-----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
	Shell Entity Class
-------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------*/

// Shell constructor intialises shell-specific data and passes its parameters to the base
// class constructor
CShellEntity::CShellEntity
(
	CEntityTemplate* entityTemplate,
	TEntityUID       UID,
	TUInt32          team,
	TEntityUID       from,
	TUInt32          damage,
	const string&    name /*=""*/,
	const CVector3&  position /*= CVector3::kOrigin*/, 
	const CVector3&  rotation /*= CVector3( 0.0f, 0.0f, 0.0f )*/,
	const CVector3&  scale /*= CVector3( 1.0f, 1.0f, 1.0f )*/
) : CEntity( entityTemplate, UID, name, position, rotation, scale )
{
	m_Speed = 100.0f;
	m_Timer = 0.0f;
	m_Timeout = 2.0f;
	m_Team = team;
	m_HitRadius = 10.0f;
	m_From = from;
	m_Damage = damage;
}


// Update the shell - controls its behaviour. The shell code is empty, it needs to be written as
// one of the assignment requirements
// Return false if the entity is to be destroyed
bool CShellEntity::Update( TFloat32 updateTime )
{
	// Move and update the time of the shell
	Matrix().MoveLocalZ(m_Speed * updateTime);
	m_Timer += updateTime;

	// If timed out, kill the shell
	if (m_Timer > m_Timeout)
	{
		return false;
	}

	EntityManager.BeginEnumEntities("", "", "Tank");
	CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
	while (tankEntity != NULL)
	{
		if (m_Team != tankEntity->GetTeam())
		{
			// If the shell is within a radius of a tank
			if (Position().DistanceToSquared(tankEntity->Position()) < m_HitRadius)
			{
				SMessage msg;
				msg.from = this->GetUID();
				msg.type = Msg_Hit;
				msg.shootAt = m_From;
				msg.damage = m_Damage;
				Messenger.SendMessageA(tankEntity->GetUID(), msg);

				EntityManager.EndEnumEntities();
				return false;
			}
		}
		tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
	}
	EntityManager.EndEnumEntities();

	return true;
}


} // namespace gen
