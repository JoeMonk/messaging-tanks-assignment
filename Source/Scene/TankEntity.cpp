/*******************************************
	TankEntity.cpp

	Tank entity template and entity classes
********************************************/

// Additional technical notes for the assignment:
// - Each tank has a team number (0 or 1), HP and other instance data - see the end of TankEntity.h
//   You will need to add other instance data suitable for the assignment requirements
// - A function GetTankUID is defined in TankAssignment.cpp and made available here, which returns
//   the UID of the tank on a given team. This can be used to get the enemy tank UID
// - Tanks have three parts: the root, the body and the turret. Each part has its own matrix, which
//   can be accessed with the Matrix function - root: Matrix(), body: Matrix(1), turret: Matrix(2)
//   However, the body and turret matrix are relative to the root's matrix - so to get the actual 
//   world matrix of the body, for example, we must multiply: Matrix(1) * Matrix()
// - Vector facing work similar to the car tag lab will be needed for the turret->enemy facing 
//   requirements for the Patrol and Aim states
// - The CMatrix4x4 function DecomposeAffineEuler allows you to extract the x,y & z rotations
//   of a matrix. This can be used on the *relative* turret matrix to help in rotating it to face
//   forwards in Evade state
// - The CShellEntity class is simply an outline. To support shell firing, you will need to add
//   member data to it and rewrite its constructor & update function. You will also need to update 
//   the CreateShell function in EntityManager.cpp to pass any additional constructor data required
// - Destroy an entity by returning false from its Update function - the entity manager wil perform
//   the destruction. Don't try to call DestroyEntity from within the Update function.
// - As entities can be destroyed, you must check that entity UIDs refer to existant entities, before
//   using their entity pointers. The return value from EntityManager.GetEntity will be NULL if the
//   entity no longer exists. Use this to avoid trying to target a tank that no longer exists etc.

#include "TankEntity.h"
#include "EntityManager.h"
#include "Messenger.h"
#include <sstream>
#include <string>

namespace gen
{

// Reference to entity manager from TankAssignment.cpp, allows look up of entities by name, UID etc.
// Can then access other entity's data. See the CEntityManager.h file for functions. Example:
//    CVector3 targetPos = EntityManager.GetEntity( targetUID )->GetMatrix().Position();
extern CEntityManager EntityManager;

// Messenger class for sending messages to and between entities
extern CMessenger Messenger;
/*-----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
	Tank Entity Class
-------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------*/

// Tank constructor intialises tank-specific data and passes its parameters to the base
// class constructor
CTankEntity::CTankEntity
(
CTankTemplate*  tankTemplate,
TEntityUID      UID,
TUInt32         team,
const string&   name /*=""*/,
const CVector3& position /*= CVector3::kOrigin*/,
const CVector3& rotation /*= CVector3( 0.0f, 0.0f, 0.0f )*/,
const CVector3& scale /*= CVector3( 1.0f, 1.0f, 1.0f )*/
) : CEntity(tankTemplate, UID, name, position, rotation, scale)
{
	m_TankTemplate = tankTemplate;

	// Tanks are on teams so they know who the enemy is
	m_Team = team;

	// Initialise other tank data and state
	m_Speed = 0.0f;
	m_MaxSpeed = tankTemplate->GetMaxSpeed();
	m_Acceleration = tankTemplate->GetAcceleration();
	m_TurnRate = tankTemplate->GetTurnSpeed();
	m_TurretTurnRate = tankTemplate->GetTurretTurnSpeed();
	m_HP = m_TankTemplate->GetMaxHP();
	m_Shells = m_TankTemplate->GetShells();
	m_ShellDamage = m_TankTemplate->GetShellDamage();
	m_State = Inactive;
	m_ShootCount = 1.0f;
	m_ShootCounting = false;
	m_Evading = false;
	m_ShellsFired = 0;

	m_CurDeathTime = 0.0f;
	m_TotalDeathTime = 1.0f;
	m_AnimationSpeed = 10.0f;
	
	m_NumOfPatrolPoints = 0;
	m_CurrentPatrolPoint = 0;

	m_PlayerPoint = CVector3(0.0f, 0.0f, 0.0f);
}

const string CTankEntity::StateStrings[NumStates] =
{
	"Inactive",
	"Patrol",
	"Aim",
	"Evade",
	"Dying",
	"Moving to",
	"Shooting at",
	"Helping"
};

// Update the tank - controls its behaviour. The shell code just performs some test behaviour, it
// is to be rewritten as one of the assignment requirements
// Return false if the entity is to be destroyed
bool CTankEntity::Update( TFloat32 updateTime )
{
	// Fetch any messages
	SMessage msg;
	while (Messenger.FetchMessage( GetUID(), &msg ))
	{
		// Set state variables based on received messages
		switch (msg.type)
		{
			case Msg_Start:
				m_State = Patrol;
				break;
			case Msg_Stop:
				m_State = Inactive;
				break;
			case Msg_Evade:
				m_State = Evade;
				break;
			case Msg_Hit:
				{
					m_HP -= msg.damage;

					EntityManager.BeginEnumEntities("", "", "Tank");
					CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
					while (tankEntity != NULL)
					{
						if (tankEntity->GetTeam() == m_Team)
						{
							SMessage newMsg;
							newMsg.type = Msg_Help;
							newMsg.shootAt = msg.shootAt;
							Messenger.SendMessageA(tankEntity->GetUID(), newMsg);
						}
						tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
					}
					EntityManager.EndEnumEntities();
				}
				break;
			case Msg_MoveTo:
				m_State = MovingTo;
				m_PlayerPoint = msg.target;
				break;
			case Msg_ShootAt:
				m_State = ShootAt;
				m_ShootingAt = msg.shootAt;
				break;
			case Msg_Help:
				m_State = Helping;
				m_ShootingAt = msg.shootAt;
				break;
		}
	}

	// Tank behaviour
	//Kill the tank if health is 0 or less
	if (m_HP <= 0)
	{
		m_State = Dying;
	}

	if (m_State == Inactive)
	{
		m_ShootCounting = false;
		m_Evading = false;
		m_Speed = 0;
	}
	else if ((m_State == Patrol) && (!m_NumOfPatrolPoints))
	{
		m_State = Inactive;
	}
	else if (m_State == Patrol)
	{
		TFloat32 xPos = Matrix().GetPosition().x;
		TFloat32 zPos = Matrix().GetPosition().z;
		// Move the the next patrol waypoint if on/around the current waypoint
		if (Position().DistanceToSquared(CVector3(m_PatrolRoute[m_CurrentPatrolPoint].x, Position().y, m_PatrolRoute[m_CurrentPatrolPoint].z)) < 1.0f)
		{
			m_CurrentPatrolPoint += 1;
			// Slow down at the waypoint
			m_Speed = m_Speed / 2.0f;
			// Wrap around the number of patrol points
			if (m_CurrentPatrolPoint == m_NumOfPatrolPoints)
			{
				m_CurrentPatrolPoint -= m_NumOfPatrolPoints;
			}
		}

		// Make the tank face the next waypoint
		CVector3 currentSideVector = Matrix().XAxis();
		CVector3 toPointVector = Normalise(Matrix().Position() - CVector3(m_PatrolRoute[m_CurrentPatrolPoint].x, 0.0f, m_PatrolRoute[m_CurrentPatrolPoint].z));
		TFloat32 dotProduct = Dot(currentSideVector, toPointVector);
		// Rotate the tank towards the waypoint
		if (dotProduct < -0.01f)
		{
			Matrix(0).RotateY(m_TurnRate * updateTime);
		}
		else if (dotProduct > 0.01f)
		{
			Matrix(0).RotateY(-m_TurnRate * updateTime);
		}


		if (fabsf(dotProduct) > 0.8f)
		{
			m_Speed = max(m_Speed - (m_Acceleration * updateTime), m_Acceleration / 2.0f);
		}
		else
		{
			m_Speed = min(m_Speed + (m_Acceleration * updateTime), m_MaxSpeed);
		}

		// Spin the turret around until the turret it pointing within 15' of another tank
		Matrix(2).RotateY(m_TurretTurnRate  * updateTime);

		EntityManager.BeginEnumEntities("", "", "Tank");
		CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		while (tankEntity != NULL)
		{
			if (tankEntity->GetTeam() != m_Team)
			{
				// If the turret is within 15' of a tank
				TFloat32 angle = ACos(Dot((Matrix(2) * Matrix(0)).ZAxis(), Normalise(tankEntity->Position() - Matrix().Position())));
				if (ToDegrees(angle) < 15.0f)
				{
					if (m_Shells > 0)
					{
						m_State = Aim;
					}
				}
			}
			tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		}
		EntityManager.EndEnumEntities();
	}
	else if (m_State == Aim)
	{
		// Stop moving the tank
		m_Speed = 0.0f;

		CVector3 currentSideVector = (Matrix(2) * Matrix()).XAxis();

		
		EntityManager.BeginEnumEntities("", "", "Tank");
		CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		bool enemy = false;	// To check that there's actually someone to shoot at and to not keep going through everyone
		while ((tankEntity != NULL) && (!enemy))
		{
			if (tankEntity->GetTeam() != m_Team)
			{
				enemy = true;
				CVector3 toPointVector = Normalise((Matrix(2) * Matrix()).Position() - tankEntity->Position());
				TFloat32 dotProduct = Dot(currentSideVector, toPointVector);
				if (dotProduct < -0.02f)
				{
					Matrix(2).RotateLocalY(m_TurretTurnRate * updateTime);
				}
				else if (dotProduct > 0.02f)
				{
					Matrix(2).RotateLocalY(-m_TurretTurnRate * updateTime);
				}
				else
				{
					// If currently shooting
					if (m_ShootCounting)
					{
						if (m_ShootCount >= 0.0f)
						{
							m_ShootCount -= updateTime;
						}
						else
						{
							dotProduct = Dot(CVector3().kZAxis, (Matrix(2) * Matrix()).ZAxis());
							TFloat32 facingProduct = Dot(CVector3().kZAxis, (Matrix(2) * Matrix()).XAxis());
							TFloat32 rotation = 0.0f;
							if (facingProduct < 0.00f)
							{
								rotation = ACos(dotProduct);
							}
							else
							{
								rotation = -ACos(dotProduct);
							}
							// Spawn a shell in the direction
							EntityManager.CreateShell("Shell Type 1", m_Team, GetUID(), m_ShellDamage, "Shell", (Matrix(2) * Matrix()).Position(), CVector3(0.0f, rotation, 0.0f));
							m_ShellsFired++;
							m_Shells--;
							m_ShootCounting = false;
							m_State = Evade;
						}
					}
					else
					{
						m_ShootCounting = true;
						m_ShootCount = 1.0f;
					}
				}
			}
			tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		}
		EntityManager.EndEnumEntities();
		// If the tank this tank was shooting at is no longer there, reset and evade
		if (!enemy)
		{
			m_ShootCounting = false;
			m_State = Evade;
		}
	}
	else if (m_State == Evade)
	{
		if (m_Evading)
		{
			TFloat32 xPos = Matrix().GetPosition().x;
			TFloat32 zPos = Matrix().GetPosition().z;
			// Move the the next patrol waypoint if on/around the current waypoint
			if ((m_EvadePoint.x > xPos - 1.0f) && (m_EvadePoint.x < xPos + 1.0f) &&
				(m_EvadePoint.z > zPos - 1.0f) && (m_EvadePoint.z < zPos + 1.0f))
			{
				m_Speed = m_Speed / 2.0f;
				m_State = Patrol;
				m_Evading = false;
			}

			// Make the tank face the next waypoint
			CVector3 currentSideVector = Matrix().XAxis();
			CVector3 toPointVector = Normalise(Matrix().Position() - CVector3(m_EvadePoint.x, 0.0f, m_EvadePoint.z));
			TFloat32 dotProduct = Dot(currentSideVector, toPointVector);
			// Rotate the tank towards the waypoint
			if (dotProduct < -0.01f)
			{
				Matrix(0).RotateY(m_TurnRate * updateTime);
			}
			else if (dotProduct > 0.01f)
			{
				Matrix(0).RotateY(-m_TurnRate * updateTime);
			}

			if (fabsf(dotProduct) > 0.8f)
			{
				m_Speed = max(m_Speed - (m_Acceleration * updateTime), m_Acceleration / 2.0f);
			}
			else
			{
				m_Speed = min(m_Speed + (m_Acceleration * updateTime), m_MaxSpeed);
			}


			// Spin the turret to face forwards
			CVector3 turretXVector = (Matrix(2) * Matrix()).XAxis();
			CVector3 currentFacingVector = -Matrix().ZAxis();
			dotProduct = Dot(turretXVector, currentFacingVector);
			if (dotProduct < -0.01f)
			{
				Matrix(2).RotateY(m_TurretTurnRate * updateTime);
			}
			else if (dotProduct > 0.01f)
			{
				Matrix(2).RotateY(-m_TurretTurnRate * updateTime);
			}
		}
		else
		{
			m_Evading = true;
			// Generate a random position within 40 radius
			float randomX = Random(-40.0f, 40.0f);
			float randomZ = Random(-40.0f, 40.0f);

			while ((randomX > -10.0f) && (randomX < 10.0f))
			{
				randomX = Random(-40.0f, 40.0f);
			}
			while ((randomZ > -10.0f) && (randomZ < 10.0f))
			{
				randomZ = Random(-40.0f, 40.0f);
			}

			CVector2 point = CVector2(randomX + Matrix(0).e30, randomZ + Matrix(0).e32);
			point.Normalise();
			m_EvadePoint.x = point.x * 40.0f;
			m_EvadePoint.z = point.y * 40.0f;
			
		}
	}
	else if (m_State == MovingTo)
	{
		TFloat32 xPos = Matrix().GetPosition().x;
		TFloat32 zPos = Matrix().GetPosition().z;
		// Move the the next patrol waypoint if on/around the current waypoint
		if (Position().DistanceToSquared(CVector3(m_PlayerPoint.x, Position().y, m_PlayerPoint.z)) < 1.0f)
		{
			// Slow down at the waypoint
			m_Speed = m_Speed / 2.0f;
			m_State = Patrol;
		}

		// Make the tank face the next waypoint
		CVector3 currentSideVector = Matrix().XAxis();
		CVector3 toPointVector = Normalise(Matrix().Position() - CVector3(m_PlayerPoint.x, Position().y, m_PlayerPoint.z));
		TFloat32 dotProduct = Dot(currentSideVector, toPointVector);
		// Rotate the tank towards the waypoint
		if (dotProduct < -0.01f)
		{
			Matrix(0).RotateY(m_TurnRate * updateTime);
		}
		else if (dotProduct > 0.01f)
		{
			Matrix(0).RotateY(-m_TurnRate * updateTime);
		}

		// Speed up if not at max speed
		m_Speed = min(m_Speed + (m_Acceleration * updateTime), m_MaxSpeed);
	}
	else if (m_State == ShootAt)
	{
		// Stop moving the tank
		m_Speed = 0.0f;

		CVector3 currentSideVector = (Matrix(2) * Matrix()).XAxis();

		CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.GetEntity(m_ShootingAt));
		if (tankEntity != NULL)
		{
			CVector3 toPointVector = Normalise((Matrix(2) * Matrix()).Position() - tankEntity->Position());
			TFloat32 dotProduct = Dot(currentSideVector, toPointVector);
			if (dotProduct < -0.02f)
			{
				Matrix(2).RotateLocalY(m_TurretTurnRate * updateTime);
			}
			else if (dotProduct > 0.02f)
			{
				Matrix(2).RotateLocalY(-m_TurretTurnRate * updateTime);
			}
			else
			{
				if (m_ShootCounting)
				{
					if (m_ShootCount >= 0.0f)
					{
						m_ShootCount -= updateTime;
					}
					else
					{
						dotProduct = Dot(CVector3().kZAxis, (Matrix(2) * Matrix()).ZAxis());
						TFloat32 facingProduct = Dot(CVector3().kZAxis, (Matrix(2) * Matrix()).XAxis());
						TFloat32 rotation = 0.0f;
						if (facingProduct < 0.00f)
						{
							rotation = ACos(dotProduct);
						}
						else
						{
							rotation = -ACos(dotProduct);
						}
						EntityManager.CreateShell("Shell Type 1", m_Team, GetUID(), m_ShellDamage, "Shell", (Matrix(2) * Matrix()).Position(), CVector3(0.0f, rotation, 0.0f));
						m_ShellsFired++;
						m_Shells--;
						m_ShootCounting = false;
						m_State = Evade;
					}
				}
				else
				{
					m_ShootCounting = true;
					m_ShootCount = 1.0f;
				}
			}
		}
		
		// If the tank this tank was shooting at is no longer there, reset and evade
		else
		{
			m_ShootCounting = false;
			m_State = Evade;
		}
	}
	else if (m_State == Helping)
	{
		// Stop moving the tank
		m_Speed = 0.0f;

		CVector3 currentSideVector = (Matrix(2) * Matrix()).XAxis();

		CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.GetEntity(m_ShootingAt));
		if (tankEntity != NULL)
		{
			CVector3 toPointVector = Normalise((Matrix(2) * Matrix()).Position() - tankEntity->Position());
			TFloat32 dotProduct = Dot(currentSideVector, toPointVector);
			if (dotProduct < -0.02f)
			{
				Matrix(2).RotateLocalY(m_TurretTurnRate * updateTime);
			}
			else if (dotProduct > 0.02f)
			{
				Matrix(2).RotateLocalY(-m_TurretTurnRate * updateTime);
			}
			else
			{
				if (m_ShootCounting)
				{
					if (m_ShootCount >= 0.0f)
					{
						m_ShootCount -= updateTime;
					}
					else
					{
						dotProduct = Dot(CVector3().kZAxis, (Matrix(2) * Matrix()).ZAxis());
						TFloat32 facingProduct = Dot(CVector3().kZAxis, (Matrix(2) * Matrix()).XAxis());
						TFloat32 rotation = 0.0f;
						if (facingProduct < 0.00f)
						{
							rotation = ACos(dotProduct);
						}
						else
						{
							rotation = -ACos(dotProduct);
						}
						EntityManager.CreateShell("Shell Type 1", m_Team, GetUID(), m_ShellDamage, "Shell", (Matrix(2) * Matrix()).Position(), CVector3(0.0f, rotation, 0.0f));
						m_ShellsFired++;
						m_Shells--;
						m_ShootCounting = false;
						m_State = Evade;
					}
				}
				else
				{
					m_ShootCounting = true;
					m_ShootCount = 1.0f;
				}
			}
		}

		// If the tank this tank was shooting at is no longer there, reset and evade
		else
		{
			m_ShootCounting = false;
			m_State = Evade;
		}
	}

	else if (m_State == Dying)
	{
		m_Speed = 0.0f;
		m_CurDeathTime += updateTime;

		Matrix(2).MoveY(m_AnimationSpeed * updateTime * -sin(m_CurDeathTime - 0.4f) * 2.0f);
		Matrix(2).MoveLocalX(m_AnimationSpeed * updateTime * 0.5f);
		Matrix(2).RotateLocalX(updateTime * sin(30.0f));
		
		Matrix(1).RotateLocalZ(updateTime * sin(30.0f));

		if (m_CurDeathTime >= m_TotalDeathTime)
		{
			return false;
		}
	}
	// Perform movement...
	// Move along local Z axis scaled by update time
	Matrix().MoveLocalZ( m_Speed * updateTime );

	return true; // Don't destroy the entity
}


} // namespace gen