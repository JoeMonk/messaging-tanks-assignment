/*******************************************
	TankEntity.h

	Tank entity template and entity classes
********************************************/

#pragma once

#include <string>
using namespace std;

#include "Defines.h"
#include "CVector3.h"
#include "Entity.h"

namespace gen
{

/*-----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
	Tank Template Class
-------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------*/

// A tank template inherits the type, name and mesh from the base template and adds further
// tank specifications
class CTankTemplate : public CEntityTemplate
{
/////////////////////////////////////
//	Constructors/Destructors
public:
	// Tank entity template constructor sets up the tank specifications - speed, acceleration and
	// turn speed and passes the other parameters to construct the base class
	CTankTemplate
	(
		const string& type, const string& name, const string& meshFilename,
		TFloat32 maxSpeed, TFloat32 acceleration, TFloat32 turnSpeed,
		TFloat32 turretTurnSpeed, TUInt32 maxHP, TUInt32 shellDamage, TUInt32 shells
	) : CEntityTemplate( type, name, meshFilename )
	{
		// Set tank template values
		m_MaxSpeed = maxSpeed;
		m_Acceleration = acceleration;
		m_TurnSpeed = turnSpeed;
		m_TurretTurnSpeed = turretTurnSpeed;
		m_MaxHP = maxHP;
		m_ShellDamage = shellDamage;
		m_Shells = shells;
	}

	// No destructor needed (base class one will do)


/////////////////////////////////////
//	Public interface
public:

	/////////////////////////////////////
	//	Getters

	TFloat32 GetMaxSpeed()
	{
		return m_MaxSpeed;
	}

	TFloat32 GetAcceleration()
	{
		return m_Acceleration;
	}

	TFloat32 GetTurnSpeed()
	{
		return m_TurnSpeed;
	}

	TFloat32 GetTurretTurnSpeed()
	{
		return m_TurretTurnSpeed;
	}

	TInt32 GetMaxHP()
	{
		return m_MaxHP;
	}

	TInt32 GetShellDamage()
	{
		return m_ShellDamage;
	}

	TUInt32 GetShells()
	{
		return m_Shells;
	}


/////////////////////////////////////
//	Private interface
private:

	// Common statistics for this tank type (template)
	TFloat32 m_MaxSpeed;        // Maximum speed for this kind of tank
	TFloat32 m_Acceleration;    // Acceleration  -"-
	TFloat32 m_TurnSpeed;       // Turn speed    -"-
	TFloat32 m_TurretTurnSpeed; // Turret turn speed    -"-

	TUInt32  m_MaxHP;           // Maximum (initial) HP for this kind of tank
	TUInt32  m_ShellDamage;     // HP damage caused by shells from this kind of tank
	TUInt32  m_Shells;
};



/*-----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
	Tank Entity Class
-------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------*/

// A tank entity inherits the ID/positioning/rendering support of the base entity class
// and adds instance and state data. It overrides the update function to perform the tank
// entity behaviour
// The shell code performs very limited behaviour to be rewritten as one of the assignment
// requirements. You may wish to alter other parts of the class to suit your game additions
// E.g extra member variables, constructor parameters, getters etc.
class CTankEntity : public CEntity
{
/////////////////////////////////////
//	Constructors/Destructors
public:
	// Tank constructor intialises tank-specific data and passes its parameters to the base
	// class constructor
	CTankEntity
	(
		CTankTemplate*  tankTemplate,
		TEntityUID      UID,
		TUInt32         team,
		const string&   name = "",
		const CVector3& position = CVector3::kOrigin, 
		const CVector3& rotation = CVector3( 0.0f, 0.0f, 0.0f ),
		const CVector3& scale = CVector3( 1.0f, 1.0f, 1.0f )
	);

	// No destructor needed


/////////////////////////////////////
//	Public interface
public:

	/////////////////////////////////////
	// Getters

	TFloat32 GetSpeed()
	{
		return m_Speed;
	}

	TUInt32 GetShellsFired()
	{
		return m_ShellsFired;
	}

	TUInt32 GetShellsLeft()
	{
		return m_Shells;
	}

	string GetCurrentState()
	{
		return StateStrings[m_State];
	}

	TInt32 GetHP()
	{
		return m_HP;
	}

	TUInt32 GetTeam()
	{
		return m_Team;
	}

	bool AddPatrolPoint(TFloat32 x, TFloat32 z)
	{
		SPatrolPosition pos;
		pos.x = x;
		pos.z = z;
		m_PatrolRoute.push_back(pos);
		m_NumOfPatrolPoints++;
		return true;
	}

	/////////////////////////////////////
	// Update

	// Update the tank - performs tank message processing and behaviour
	// Return false if the entity is to be destroyed
	// Keep as a virtual function in case of further derivation
	virtual bool Update( TFloat32 updateTime );
	

/////////////////////////////////////
//	Private interface
private:

	/////////////////////////////////////
	// Types

	// States available for a tank - placeholders for shell code
	// Any state added, remember to add to the state strings array
	enum EState
	{
		Inactive,
		Patrol,
		Aim,
		Evade,
		Dying,
		MovingTo,
		ShootAt,
		Helping,
		NumStates
	};
	// States in string form, for writing out instead of enumerated
	// Set in the CPP
	static const string StateStrings[NumStates];

	struct SPatrolPosition
	{
		TFloat32 x;
		TFloat32 z;
	};


	/////////////////////////////////////
	// Data

	// The template holding common data for all tank entities
	CTankTemplate* m_TankTemplate;

	// Tank data
	TUInt32  m_Team;			// Team number for tank (to know who the enemy is)
	TFloat32 m_Speed;			// Current speed (in facing direction)
	TFloat32 m_MaxSpeed;		// The max speed the tank moves
	TFloat32 m_Acceleration;	// How fast the tank accelerates
	TFloat32 m_TurnRate;		// How fast the tank turns
	TFloat32 m_TurretTurnRate;	// How fast the turret turns
	TInt32   m_HP;				// Current hit points for the tank
	TFloat32 m_ShootCount;		// The count down until the turret fires
	bool     m_ShootCounting;	// Are we currently counting down to shoot
	TUInt32  m_ShellsFired;	// The number of shells fired
	TUInt32  m_Shells;
	TUInt32  m_ShellDamage;

	TFloat32 m_CurDeathTime;		// Current death animation time
	TFloat32 m_TotalDeathTime;		// Time for the full death animation time
	TFloat32 m_AnimationSpeed;		// Speed of the moving parts

	CVector3 m_PlayerPoint;			// Where the player moves the tank
	TEntityUID m_ShootingAt;		// The tank the player chooses to shoot at

	// Tank state
	EState   m_State;						// Current state
	vector<SPatrolPosition> m_PatrolRoute;	//Contains all the patrol points
	TInt32   m_CurrentPatrolPoint;	        //Where the tank it currently moving towards
	TInt32   m_NumOfPatrolPoints;	        //The number of patrol points
	SPatrolPosition m_EvadePoint;
	bool m_Evading;
};


} // namespace gen
