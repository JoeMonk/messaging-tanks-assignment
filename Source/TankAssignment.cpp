/*******************************************
	TankAssignment.cpp

	Shell scene and game functions
********************************************/

#include <sstream>
#include <string>
using namespace std;

#include <d3dx9.h>

#include "Defines.h"
#include "CVector3.h"
#include "Camera.h"
#include "Light.h"
#include "EntityManager.h"
#include "Messenger.h"
#include "TankAssignment.h"
#include "CParseLevel.h"

namespace gen
{

	//-----------------------------------------------------------------------------
	// Game / scene constants
	//-----------------------------------------------------------------------------

	// Fixed array sizes
	const int NumLights = 2;

	// Control speed
	const float CameraRotSpeed = 2.0f;
	float CameraMoveSpeed = 40.0f;

	// Shared / set by the main update code and needed by the text render code, should be moved into the tank class

	TEntityUID SelectedTank = NULL;	// The selected tank
	TEntityUID NearestTank = NULL;	// The nearest tank to the cursor

	//-----------------------------------------------------------------------------
	// Global system variables
	//-----------------------------------------------------------------------------

	// Get reference to global DirectX variables from another source file
	extern LPDIRECT3DDEVICE9 g_pd3dDevice;
	extern LPD3DXFONT        g_pFont;

	// Actual viewport dimensions (fullscreen or windowed)
	extern TUInt32 ViewportWidth;
	extern TUInt32 ViewportHeight;

	// Current mouse position
	extern TUInt32 MouseX;
	extern TUInt32 MouseY;

	// Messenger class for sending messages to and between entities
	extern CMessenger Messenger;


	//-----------------------------------------------------------------------------
	// Global game/scene variables
	//-----------------------------------------------------------------------------

	// Entity manager
	CEntityManager EntityManager;
	CParseLevel LevelParser(&EntityManager);

	// Other scene elements
	SColourRGBA AmbientLight;
	CLight* Lights[NumLights];
	CCamera* MainCamera;

	//-----------------------------------------------------------------------------
	// Scene management
	//-----------------------------------------------------------------------------

	// Creates the scene geometry
	bool SceneSetup()
	{
		//////////////////////////////////////////
		// Create scenery templates and entities
		LevelParser.ParseFile("Entities.xml");

		/////////////////////////////
		// Camera / light setup

		// Set camera position and clip planes
		MainCamera = new CCamera(CVector3(0.0f, 30.0f, -100.0f),
			CVector3(ToRadians(15.0f), 0, 0), 1.0f, 20000.0f, kfPi / 3.0f, ViewportWidth / ViewportHeight);


		// Ambient light level
		AmbientLight = SColourRGBA(0.6f, 0.6f, 0.6f, 1.0f);

		// Sunlight
		Lights[0] = new CLight(CVector3(-5000.0f, 4000.0f, -10000.0f),
			SColourRGBA(1.0f, 0.9f, 0.6f), 40000.0f);
		Lights[1] = new CLight(CVector3(6.0f, 7.5f, 40.0f),
			SColourRGBA(1.0f, 0.0f, 0.0f), 1.0f);


		// Set light information for render methods
		SetAmbientLight(AmbientLight);
		SetLights(&Lights[0]);

		return true;
	}


	// Release everything in the scene
	void SceneShutdown()
	{
		// Release render methods
		ReleaseMethods();

		// Release lights
		for (int light = NumLights - 1; light >= 0; --light)
		{
			delete Lights[light];
		}

		// Release camera
		delete MainCamera;

		// Destroy all entities / templates
		EntityManager.DestroyAllEntities();
		EntityManager.DestroyAllTemplates();
	}


	//-----------------------------------------------------------------------------
	// Game loop functions
	//-----------------------------------------------------------------------------

	// Draw one frame of the scene
	void RenderScene(float updateTime)
	{
		// Begin the scene
		if (SUCCEEDED(g_pd3dDevice->BeginScene()))
		{
			// Clear the z buffer & stencil buffer - no need to clear back-buffer 
			// as the skybox will always fill the background
			g_pd3dDevice->Clear(0, NULL, D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL,
				0, 1.0f, 0);

			// Prepare camera
			MainCamera->CalculateMatrices();

			// Render all entities
			EntityManager.RenderAllEntities(MainCamera);

			// Draw on-screen text
			RenderSceneText(updateTime);

			// End the scene
			g_pd3dDevice->EndScene();
		}

		// Present the backbuffer contents to the display
		g_pd3dDevice->Present(NULL, NULL, NULL, NULL);
	}


	// Render a single text string at the given position in the given colour, may optionally centre it
	void RenderText(const string& text, int X, int Y, float r, float g, float b, bool centre = false)
	{
		RECT rect;
		if (!centre)
		{
			SetRect(&rect, X, Y, 0, 0);
			g_pFont->DrawText(NULL, text.c_str(), -1, &rect, DT_NOCLIP, D3DXCOLOR(r, g, b, 1.0f));
		}
		else
		{
			SetRect(&rect, X - 100, Y, X + 100, 0);
			g_pFont->DrawText(NULL, text.c_str(), -1, &rect,
				DT_CENTER | DT_NOCLIP, D3DXCOLOR(r, g, b, 1.0f));
		}
	}

	// Render on-screen text each frame
	void RenderSceneText(float updateTime)
	{
		// Write FPS text string
		stringstream outText;
		outText << "Frame Time: " << updateTime * 1000.0f << "ms" << endl << "FPS:" << 1.0f / updateTime;
		RenderText(outText.str(), 0, 0, 1.0f, 1.0f, 0.0f);
		outText.str("");

		static bool moreInfo = false;

		if (KeyHit(Key_0))
		{
			moreInfo = !moreInfo;
		}

		TInt32 X, Y;
		EntityManager.BeginEnumEntities("", "", "Tank");
		CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		while (tankEntity != NULL)
		{
			//Display Text
			if (MainCamera->PixelFromWorldPt(tankEntity->Position(), ViewportWidth, ViewportHeight, &X, &Y))
			{
				if (!moreInfo)
				{
					outText << tankEntity->Template()->GetName().c_str();
				}
				else
				{
					outText << tankEntity->Template()->GetName().c_str() << endl;
					outText << "HP: " << tankEntity->GetHP() << endl;
					outText << "Current state: " << tankEntity->GetCurrentState() << endl;
					outText << "Shells fired: " << tankEntity->GetShellsFired() << endl;
					outText << "Shells left: " << tankEntity->GetShellsLeft();
				}
				if (tankEntity->GetUID() == SelectedTank)
				{
					RenderText(outText.str(), X, Y, 1.0f, 0.0f, 0.0f, true);
				}
				else if (tankEntity->GetUID() == NearestTank)
				{
					RenderText(outText.str(), X, Y, 1.0f, 1.0f, 0.0f, true);
				}
				else
				{
					RenderText(outText.str(), X, Y, 1.0f, 1.0f, 1.0f, true);
				}
				outText.str("");
			}
			tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		}
		EntityManager.EndEnumEntities();
	}


	// Update the scene between rendering
	void UpdateScene(float updateTime)
	{
		// Call all entity update functions
		EntityManager.UpdateAllEntities(updateTime);


		// Set camera speeds
		// Key F1 used for full screen toggle
		if (KeyHit(Key_F2)) CameraMoveSpeed = 5.0f;
		if (KeyHit(Key_F3)) CameraMoveSpeed = 40.0f;

		// Run through all the tank entities and send them a start message
		if (KeyHit(Key_1))
		{
			SMessage msg;
			msg.from = -1;
			msg.type = Msg_Start;

			EntityManager.BeginEnumEntities("", "", "Tank");
			CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
			while (tankEntity != NULL)
			{
				Messenger.SendMessageA(tankEntity->GetUID(), msg);
				tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
			}
			EntityManager.EndEnumEntities();
		}

		// Run through all the tank entities and send them a stop message
		if (KeyHit(Key_2))
		{
			SMessage msg;
			msg.from = -1;
			msg.type = Msg_Stop;

			EntityManager.BeginEnumEntities("", "", "Tank");
			CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
			while (tankEntity != NULL)
			{
				Messenger.SendMessageA(tankEntity->GetUID(), msg);
				tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
			}
			EntityManager.EndEnumEntities();
		}

		// Find the closest tank to the cursor, set it to closest tank. Null if no tank is within nearestDistance range.
		EntityManager.BeginEnumEntities("", "", "Tank");
		CTankEntity* tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		CTankEntity* closestTank = NULL;
		TInt32 X, Y;
		CVector2 mousePixel = CVector2(TFloat32(MouseX), TFloat32(MouseY));
		float nearestDistance = 40.0f;
		while (tankEntity != NULL)
		{
			if (MainCamera->PixelFromWorldPt(tankEntity->Position(), ViewportWidth, ViewportHeight, &X, &Y))
			{
				float pixelDistance = Distance(mousePixel, CVector2(TFloat32(X), TFloat32(Y)));
				if (pixelDistance < nearestDistance)
				{
					nearestDistance = pixelDistance;
					closestTank = tankEntity;
				}
			}
			tankEntity = static_cast<CTankEntity*>(EntityManager.EnumEntity());
		}
		EntityManager.EndEnumEntities();

		// Set the global closest, for the text rendering
		if (closestTank)
		{
			NearestTank = closestTank->GetUID();
		}
		else
		{
			NearestTank = NULL;
		}

		// Send a message if clicked on and not already the selected tank

		if ((KeyHit(Mouse_LButton)) && (closestTank != NULL))
		{
			if (SelectedTank != closestTank->GetUID())
			{
				SelectedTank = closestTank->GetUID();
			}
		}

		// Tell the selected tank to evade
		if (EntityManager.GetEntity(SelectedTank) && (KeyHit(Key_E)))
		{
			SMessage msg;
			msg.from = -1;
			msg.type = Msg_Evade;
			Messenger.SendMessageA(SelectedTank, msg);
		}

		// Chase cam stuff, only do chase cam if there is a tank selected
		static bool ChaseCam = false;

		if (KeyHit(Key_C) && EntityManager.GetEntity(SelectedTank))
		{
			ChaseCam = !ChaseCam;
		}

		if (!ChaseCam)
		{
			// Move the camera
			MainCamera->Control(Key_Up, Key_Down, Key_Left, Key_Right, Key_W, Key_S, Key_A, Key_D,
				CameraMoveSpeed * updateTime, CameraRotSpeed * updateTime);
		}
		else
		{
			CTankEntity* tank = static_cast<CTankEntity*>(EntityManager.GetEntity(SelectedTank));
			MainCamera->Position() = tank->Position() + CVector3(0.0f, 8.0f, 0.0f) + (tank->Matrix().ZAxis() * -12.0f);
			MainCamera->Matrix().FaceTarget(tank->Position() + CVector3(0.0, 5.0f, 0.0f));
		}

		// Manually move a tank
		if ((KeyHit(Mouse_RButton)) && (EntityManager.GetEntity(SelectedTank)))
		{
			// If a tank is highlighted
			if ((closestTank) && (SelectedTank != closestTank->GetUID()))
			{
				SMessage msg;
				msg.from = -1;
				msg.type = Msg_ShootAt;
				msg.shootAt = closestTank->GetUID();

				Messenger.SendMessageA(SelectedTank, msg);
			}
			// If no tank is highlighted, click the ground
			else
			{
				
				CVector3 worldPt = MainCamera->WorldPtFromPixel(MouseX, MouseY, ViewportWidth, ViewportHeight);

				CVector3 ray = Normalise(MainCamera->Position() - worldPt);

				TFloat32 dist = worldPt.y / ray.y;

				CVector3 groundPoint = MainCamera->Position() - ray * dist;

				SMessage msg;
				msg.from = -1;
				msg.type = Msg_MoveTo;
				msg.target = groundPoint;

				Messenger.SendMessageA(SelectedTank, msg);
			}
		}
	}
}