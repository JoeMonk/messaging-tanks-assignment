///////////////////////////////////////////////////////////
//  CParseLevel.cpp
//  A class to parse and setup a level (entity templates
//  and instances) from an XML file
//  Created on:      30-Jul-2005 14:40:00
//  Original author: LN
///////////////////////////////////////////////////////////

#include "BaseMath.h"
#include "Entity.h"
#include "TankEntity.h"
#include "CParseLevel.h"
#include "BaseMath.h"

namespace gen
{

/*---------------------------------------------------------------------------------------------
	Constructors / Destructors
---------------------------------------------------------------------------------------------*/

// Constructor initialises state variables
CParseLevel::CParseLevel( CEntityManager* entityManager )
{
	// Take copy of entity manager for creation
	m_EntityManager = entityManager;

	// File state
	m_CurrentSection = None;

	// Template state
	m_TemplateType = "";
	m_TemplateName = "";
	m_TemplateMesh = "";

	m_MaxSpeed = 0.0f;
	m_Acceleration = 0.0f;
	m_TurnSpeed = 0.0f;
	m_TurretTurnSpeed = 0.0f;
	m_MaxHP = 0;
	m_ShellDamage = 0;
	m_Shells = 0;

	// Entity state
	m_EntityType = "";
	m_EntityName = "";
	m_Pos = CVector3::kOrigin;
	m_Rot = CVector3::kZero;
	m_Scale = CVector3(1.0f, 1.0f, 1.0f);

	m_Team = -1;

	m_Count = 0;
	m_PosRand = CVector3::kZero;
	m_RotRand = CVector3::kZero;

	m_PatrolPoints.clear();
}


/*---------------------------------------------------------------------------------------------
	Callback Functions
---------------------------------------------------------------------------------------------*/

// Callback function called when the parser meets the start of a new element (the opening tag).
// The element name is passed as a string. The attributes are passed as a list of (C-style)
// string pairs: attribute name, attribute value. The last attribute is marked with a null name
void CParseLevel::StartElt( const string& eltName, SAttribute* attrs )
{
	// Open major file sections
	if (eltName == "Templates")
	{
		m_CurrentSection = Templates;
	}
	else if (eltName == "Entities")
	{
		m_CurrentSection = Entities;
	}

	// Different parsing depending on section currently being read
	switch (m_CurrentSection)
	{
		case Templates:
			TemplatesStartElt( eltName, attrs ); // Parse template start elements
			break;
		case Entities:
			EntitiesStartElt( eltName, attrs ); // Parse entity start elements
			break;
	}
}

// Callback function called when the parser meets the end of an element (the closing tag). The
// element name is passed as a string
void CParseLevel::EndElt( const string& eltName )
{
	// Close major file sections
	if (eltName == "Templates" || eltName == "Entities")
	{
		m_CurrentSection = None;
	}

	// Different parsing depending on section currently being read
	switch (m_CurrentSection)
	{
		case Templates:
			TemplatesEndElt( eltName ); // Parse template end elements
			break;
		case Entities:
			EntitiesEndElt( eltName ); // Parse entity end elements
			break;
	}
}


/*---------------------------------------------------------------------------------------------
	Section Parsing
---------------------------------------------------------------------------------------------*/

// Called when the parser meets the start of an element (opening tag) in the templates section
void CParseLevel::TemplatesStartElt( const string& eltName, SAttribute* attrs )
{
	// Started reading a new entity template - get type, name and mesh
	if (eltName == "EntityTemplate")
	{
		// Get attributes held in the tag
		m_TemplateType = GetAttribute( attrs, "Type" );
		m_TemplateName = GetAttribute( attrs, "Name" );
		m_TemplateMesh = GetAttribute( attrs, "Mesh" );

		// Get further template data if this is a ship template
		if (m_TemplateType == "Tank")
		{
			// Get additional attributes held in ship template tags
			m_MaxHP = GetAttributeInt(attrs, "HP");
			m_MaxSpeed = GetAttributeFloat(attrs, "MaxSpeed");
			m_Acceleration = GetAttributeFloat(attrs, "Acceleration");
			m_TurnSpeed = GetAttributeFloat(attrs, "TurnSpeed");
			m_TurretTurnSpeed = gen::kfPi / GetAttributeFloat(attrs, "TurretTurnSpeed");
			m_ShellDamage = GetAttributeInt(attrs, "ShellDamage");
			m_Shells = GetAttributeInt(attrs, "Shells");
		}
	}
}

// Called when the parser meets the end of an element (closing tag) in the templates section
void CParseLevel::TemplatesEndElt( const string& eltName )
{
	// Finished reading an entity template - create it using parsed data
	if (eltName == "EntityTemplate")
	{
		CreateEntityTemplate();
	}
}


// Called when the parser meets the start of an element (opening tag) in the entities section
void CParseLevel::EntitiesStartElt( const string& eltName, SAttribute* attrs )
{
	// Started reading a team of entities - get team data
	if (eltName == "Team")
	{
		m_Team = GetAttributeInt( attrs, "Name" );
		string colourString = GetAttribute( attrs, "Colour" );
	}

	// Started reading a new entity - get type and name
	else if (eltName == "Entity")
	{
		m_EntityType = GetAttribute( attrs, "Type" );
		m_EntityName = GetAttribute( attrs, "Name" );

		// Set default positions
		m_Pos = CVector3::kOrigin;
		m_Rot = CVector3 (0.0f, 0.0f, 0.0f);
		m_Scale = CVector3(1.0f, 1.0f, 1.0f);
	}

	// Started reading an entity position - get X,Y,Z
	if (eltName == "Position")
	{
		m_Pos.x = GetAttributeFloat( attrs, "X" );
		m_Pos.y = GetAttributeFloat( attrs, "Y" );
		m_Pos.z = GetAttributeFloat( attrs, "Z" );
	}
	// Started reading an entity rotation - get X,Y,Z
	else if (eltName == "Rotation")
	{
		m_Rot.x = ToRadians(GetAttributeFloat( attrs, "X" ));
		m_Rot.y = ToRadians(GetAttributeFloat( attrs, "Y" ));
		m_Rot.z = ToRadians(GetAttributeFloat( attrs, "Z" ));
	}
	// Started reading an entity scale - get X,Y,Z
	else if (eltName == "Scale")
	{
		m_Scale.x = GetAttributeFloat(attrs, "X");
		m_Scale.y = GetAttributeFloat(attrs, "Y");
		m_Scale.z = GetAttributeFloat(attrs, "Z");
	}

	else if (eltName == "Count")
	{
		m_Count = GetAttributeInt(attrs, "Value");
	}
	// Randomising an entity position - get X,Y,Z amounts and randomise
	else if (eltName == "PosRandomise")
	{
		m_PosRand.x = GetAttributeFloat(attrs, "X") * 0.5f;
		m_PosRand.y = GetAttributeFloat(attrs, "Y") * 0.5f;
		m_PosRand.z = GetAttributeFloat(attrs, "Z") * 0.5f;
	}
	else if (eltName == "RotRandomise")
	{
		m_RotRand.x = GetAttributeFloat(attrs, "X") * 0.5f;
		m_RotRand.y = GetAttributeFloat(attrs, "Y") * 0.5f;
		m_RotRand.z = GetAttributeFloat(attrs, "Z") * 0.5f;
	}
	else if (eltName == "PatrolPoint")
	{
		CVector2 point;
		point.x = GetAttributeFloat(attrs, "X");
		point.y = GetAttributeFloat(attrs, "Z");
		m_PatrolPoints.push_back(point);
	}
	else if (eltName == "PPRandomise")
	{
		CVector2 randomNum;
		randomNum.x = GetAttributeFloat(attrs, "X") * 0.5f;
		randomNum.y = GetAttributeFloat(attrs, "Z") * 0.5f;
		for (int i = 0; i < m_PatrolPoints.size(); i++)
		{
			m_PatrolPoints[i].x += Random(-randomNum.x, randomNum.x);
			m_PatrolPoints[i].y += Random(-randomNum.y, randomNum.y);
		}
	}
}

// Called when the parser meets the end of an element (closing tag) in the entities section
void CParseLevel::EntitiesEndElt( const string& eltName )
{
	// Finished reading a team - reset team data
	if (eltName == "Team")
	{
		m_Team = 0;
	}

	// Finished reading entity - create it using parsed data
	else if (eltName == "Entity")
	{
		CreateEntity();
	}
}


/*---------------------------------------------------------------------------------------------
	Entity Template and Instance Creation
---------------------------------------------------------------------------------------------*/

// Create an entity template using data collected from parsed XML elements
void CParseLevel::CreateEntityTemplate()
{
	// Initialise the template depending on its type
	if (m_TemplateType == "Tank")
	{
		m_EntityManager->CreateTankTemplate( m_TemplateType, m_TemplateName, m_TemplateMesh, m_MaxSpeed,
										m_Acceleration, m_TurnSpeed, m_TurretTurnSpeed, m_MaxHP, m_ShellDamage, m_Shells);
	}
	else
	{
		// Generic template
		m_EntityManager->CreateTemplate( m_TemplateType, m_TemplateName, m_TemplateMesh );
	}
}

// Create an entity using data collected from parsed XML elements
void CParseLevel::CreateEntity()
{
	// Get the type of template that this entity uses
	string templateType = m_EntityManager->GetTemplate( m_EntityType )->GetType();

	// Create a new entity of this template type - commit the settings that have been read since the opening tag
	if (templateType == "Tank")
	{
		TEntityUID tank = m_EntityManager->CreateTank(m_EntityType, m_Team, m_EntityName, m_Pos, m_Rot, m_Scale);
		CTankEntity* tankEntity = static_cast<CTankEntity*>(m_EntityManager->GetEntity(tank));
		for (int i = 0; i < m_PatrolPoints.size(); i++)
		{
			tankEntity->AddPatrolPoint(m_PatrolPoints[i].x, m_PatrolPoints[i].y);
		}
	}
	else if (templateType == "Tree")
	{
		for (unsigned int i = 0; i < m_Count; i++)
		{
			CVector3 pos = m_Pos;
			pos.x += Random(-m_PosRand.x, m_PosRand.x);
			pos.z += Random(-m_PosRand.z, m_PosRand.z);

			CVector3 rot = m_Rot;
			rot.x += ToRadians(Random(-m_RotRand.x, m_RotRand.x));
			rot.z += ToRadians(Random(-m_RotRand.z, m_RotRand.z));

			m_EntityManager->CreateEntity(m_EntityType, m_EntityName, pos, rot, m_Scale);
		}
	}
	else
	{
		m_EntityManager->CreateEntity(m_EntityType, m_EntityName, m_Pos, m_Rot, m_Scale);
	}
}


} // namespace gen
